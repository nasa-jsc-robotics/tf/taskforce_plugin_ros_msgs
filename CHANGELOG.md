Change Log
============

1.0.4
------------

* Remove trusty CI

1.0.3
------------

* Added NOSA LICENSE.md file

1.0.2
------------

* Added xenial CI
